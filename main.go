package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"schneiderlein.xyz/shutdown-rest-api/restapi"
)

func handleRequests(ip *string, port *int) {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/state", restapi.ShutdownPc).Methods("POST")
	myRouter.HandleFunc("/state", restapi.GetPcState)

	listen := fmt.Sprintf("%s:%d", *ip, *port)
	fmt.Printf("Server listening at %s\n", listen)
	log.Fatal(http.ListenAndServe(listen, myRouter))
}

func main() {
	port := flag.Int("p", 18080, "listen port")
	ip := flag.String("b", "0.0.0.0", "binding ip")
	flag.Parse()

	fmt.Println("Running shutdown REST API")
	handleRequests(ip, port)
}
