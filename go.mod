module schneiderlein.xyz/shutdown-rest-api

go 1.17

require github.com/gorilla/mux v1.8.0

require (
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf // indirect
	github.com/godbus/dbus v4.1.0+incompatible // indirect
)
