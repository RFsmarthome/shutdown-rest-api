package restapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/coreos/go-systemd/login1"
)

type PcState struct {
	IsActive bool `json:"is_active"`
}

type PcAction struct {
	Active bool `json:"active"`
}

var g_isActive bool = true

// Handle get state
func GetPcState(w http.ResponseWriter, r *http.Request) {
	var pcStates = PcState{
		IsActive: g_isActive,
	}

	err := json.NewEncoder(w).Encode(pcStates)
	if err != nil {
		fmt.Println("Could not output status")
	}
}

// Shutdown PC action
func ShutdownPc(w http.ResponseWriter, r *http.Request) {
	var action PcAction

	reqBody, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(reqBody, &action)

	if err != nil {
		fmt.Println("Could not interpred request, ignoring")
		return
	}

	if !action.Active {
		g_isActive = false
		fmt.Println("Shutdown PC")
		con, _ := login1.New()
		con.PowerOff(false)
		con.Close()
	} else {
		fmt.Println("PC stayed on")
	}
}
